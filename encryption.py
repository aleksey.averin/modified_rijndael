from pyfinite import ffield

from utils import affine_transform, xor_reduce


class RijndaelEncrypt:
    def __init__(self, n, gen, affine, c=0x63):
        self.finite_field = ffield.FField(n, gen, useLUT=0)
        self.affine = affine
        self.c = c

    def encrypt(self, state, expanded_key) -> int:
        rounds_num = 14  # Число раундов
        state = self.add_round_key(state, expanded_key)

        for i in range(rounds_num - 1):
            state = self.normal_round(state, expanded_key)

        return self.final_round(state, expanded_key)

    def sub_bytes(self, state):
        print(f'{state}={self.__sbox(state)}')
        return self.__sbox(state)

    def shift_rows(self, state):
        return state

    def mix_columns(self, state):
        return state

    def add_round_key(self, state, round_key):
        return state

    def normal_round(self, state, round_key):
        state = self.sub_bytes(state)
        state = self.shift_rows(state)
        state = self.mix_columns(state)
        return self.add_round_key(state, round_key)

    def final_round(self, state, round_key):
        state = self.sub_bytes(state)  # Замена байт
        state = self.shift_rows(state)  # Сдвиг строк
        return self.add_round_key(state, round_key)  # Добавление циклового ключа

    def __sbox(self, state):
        inv = self.finite_field.Inverse(state)
        return xor_reduce(affine_transform(self.affine, inv)) ^ self.c
