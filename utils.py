import operator
from functools import reduce


def plaintext_split(plaintext: str, block_size: int = 4):
    """Разбивает строку на массив байт размером N байт"""
    byte_array = plaintext.encode()
    return [int.from_bytes(byte_array[i:i + block_size], byteorder='big', signed=False)
            for i in range(0, len(byte_array), block_size)]


def rot_r(val, r_bits, max_bits=32):
    """Циклический сдвиг вправо"""
    return ((val & (2 ** max_bits - 1)) >> r_bits % max_bits) | \
           (val << (max_bits - (r_bits % max_bits)) & (2 ** max_bits - 1))


def rot_l(val, r_bits, max_bits=32):
    """Циклический сдвиг влево"""
    return (val << r_bits % max_bits) & (2 ** max_bits - 1) | ((val & (2 ** max_bits - 1)) >> (max_bits - (r_bits % max_bits)))


def get_bit(number, bit):
    return 1 if ((number & (1 << bit)) != 0) else 0


def bits_iter(number, size=32):
    for shift in range(size):
        yield get_bit(number, shift)


def affine_matrix(affine_row: int, size=32):
    """Функция генерации матрицы афинного преобазования"""
    for i in range(size):
        yield rot_r(affine_row, i, max_bits=size)


def bits_to_int(bits):
    s = ''.join(map(str, bits))
    return int(s, 2)


def xor_reduce(items):
    return reduce(operator.xor, items)


def affine_transform(affine_row, inverse):
    for ind, bit in enumerate(reversed(affine_row), 1):
        yield rot_l(inverse, ind, max_bits=8) if bit else 0
