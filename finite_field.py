import datetime

from pyfinite import ffield


def test():
    start = datetime.datetime.now()
    finite_field = ffield.FField(32, gen=4294967437)
    for i in range(2 ** 20):
        print(f'{i}={finite_field.Inverse(i)}')
    end = datetime.datetime.now()
    print(end-start)

# int.from_bytes([255, 255, 255, 255], byteorder='big', signed=False)
