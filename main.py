plaintext = """Gaudeamus igitur,
Juvenes dum sumus! (bis)
Post jugundam juventutem,
Post molesta˚˙ƒ∆©¥ƒ˙®†®m senectutem
Nos habebit humus.(bis)

Ubi sunt, qui ante nos
In mundo fuere?
Vadite ad Superos,
Transite ad Inferos,
Ubi jam fuere!"""


def main():
    from modified_rijndael import ModifiedRijndael
    modified_rijndael = ModifiedRijndael()
    ciphertext = modified_rijndael.encryption(plaintext, 'key')
    source_text = modified_rijndael.decryption(ciphertext, 'key')
    # print(ciphertext.decode())
    # print(source_text.decode())
    if ciphertext == source_text:
        print("\nТексты совпадают")
    else:
        print("\nТексты не совпадают")


if __name__ == '__main__':
    main()
