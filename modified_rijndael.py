from decryption import RijndaelDecrypt
from encryption import RijndaelEncrypt
from utils import plaintext_split
from word import key_expansion


class ModifiedRijndael(object):
    def __init__(self, n=8, gen=283, affine=(1, 0, 0, 0, 1, 1, 1, 1), r_affine=(0, 0, 1, 0, 0, 1, 0, 1), c=0x63, c_r=0x5):
        self.encrypt = RijndaelEncrypt(n, gen, affine, c)
        self.decrypt = RijndaelDecrypt(n, gen, r_affine, c_r)

    def encryption(self, plaintext: str, key: str):
        w = key_expansion(key)  # Получаем "расписание ключей(Key Expansion)"
        blocks = plaintext_split(plaintext, 1)  # Разбиваем текст на блоки
        cipher = [i for i in [(self.encrypt.encrypt(block, w)).to_bytes(4, byteorder="big") for block in blocks]]
        return b''.join(cipher)  # Соединяем полученный шифротекст

    def decryption(self, ciphertext: str, key: str):
        return ciphertext
